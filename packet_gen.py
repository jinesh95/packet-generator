#!/usr/bin/python
'''
Packet Generator By reading Files 
Written By : Jinesh Patel

'''

import socket, sys,re,random,string
from struct import *
import os

FILE_PATH = os.path.dirname(os.path.realpath(__file__)) + '/'    #File Path where you put your file
FILE_NAME = 'header.txt'                                   # Name of File 


def checksum(msg):
#Check Sum Algorithm
    s = 0
    for i in range(0, len(msg), 2):
        w = ord(msg[i]) + (ord(msg[i+1]) << 8 )
        s = s + w
     
    s = (s>>16) + (s & 0xffff);
    s = s + (s >> 16);
     
    # 1's complement and mask to 4 byte short
    s = ~s & 0xffff
     
    return s

def packet_builder(src_ip,dst_ip,src_port_no,dst_port_no):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
    except socket.error , msg:
        print '::::Socket is not Created::::' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()

    packet = '';
 
    source_ip   =   str(src_ip)
    dest_ip     =   str(dst_ip)


# IP Packet Format
# 0                   1                   2                   3   
#    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |Version|  IHL  |Type of Service|          Total Length         |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |         Identification        |Flags|      Fragment Offset    |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |  Time to Live |    Protocol   |         Header Checksum       |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                       Source Address                          |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                    Destination Address                        |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                    Options                    |    Padding    |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#
#                    Example Internet Datagram Header


#########################IP HEADER STARTS#############################################################
    # IP HEADER FIELD PARAMS SET VARIOUS HEADER PARAMETERS 
    ip_ihl = 5
    ip_ver = 4
    ip_tos = 0
    ip_tot_len = 0 
    ip_id = random.randint(0,65536)  
    ip_frag_off = 0
    ip_ttl = 255
    ip_proto = socket.IPPROTO_TCP
    ip_check = 0    
    ip_saddr = socket.inet_aton ( source_ip )    # Source IP Address 
    ip_daddr = socket.inet_aton ( dest_ip )      # Destination IP Address
 
    ip_ihl_ver = (ip_ver << 4) + ip_ihl
    
    ip_header = pack('!BBHHHBBH4s4s' , ip_ihl_ver, ip_tos, ip_tot_len, ip_id, ip_frag_off, ip_ttl, ip_proto, ip_check, ip_saddr, ip_daddr)
    
######################################IP HEADER ENDS HERE############################################ 
######################################TCP HEADER FORMAT##############################################
#0                   1                   2                   3   
#    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |          Source Port          |       Destination Port        |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                        Sequence Number                        |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                    Acknowledgment Number                      |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |  Data |           |U|A|P|R|S|F|                               |
#   | Offset| Reserved  |R|C|S|S|Y|I|            Window             |
#   |       |           |G|K|H|T|N|N|                               |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |           Checksum            |         Urgent Pointer        |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                    Options                    |    Padding    |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#   |                             data                              |
#   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#
#                            TCP Header Format


    # tcp header fields
    tcp_source = src_port_no   # source port
    tcp_dest = dst_port_no     # destination port
    tcp_seq = random.randint(0,65536)
    tcp_ack_seq = random.randint(0,65536)
    tcp_doff = 5
    #tcp flags
    tcp_fin = 0
    tcp_syn = 1
    tcp_rst = 0
    tcp_psh = 0
    tcp_ack = 0
    tcp_urg = 0
    tcp_window = socket.htons (5840)    #   maximum allowed window size
    tcp_check = 0
    tcp_urg_ptr = 0
    
    tcp_offset_res = (tcp_doff << 4) + 0
    tcp_flags = tcp_fin + (tcp_syn << 1) + (tcp_rst << 2) + (tcp_psh <<3) + (tcp_ack << 4) + (tcp_urg << 5)
 
    # the ! in the pack format string means network order
    tcp_header = pack('!HHLLBBHHH' , tcp_source, tcp_dest, tcp_seq, tcp_ack_seq, tcp_offset_res, tcp_flags,  tcp_window, tcp_check, tcp_urg_ptr)

    
#    user_data = ''.join(random.sample(string.letters*5,5))
    user_data = 'This is Gigamon Packet'
 

    # pseudo header fields
    source_address = socket.inet_aton( source_ip )
    dest_address = socket.inet_aton(dest_ip)
    placeholder = 0
    protocol = socket.IPPROTO_TCP
    tcp_length = len(tcp_header) + len(user_data)
 
    psh = pack('!4s4sBBH' , source_address , dest_address , placeholder , protocol , tcp_length);
    psh = psh + tcp_header + user_data;
 
    tcp_check = checksum(psh)
    #print tcp_checksum
 
    # make the tcp header again and fill the correct checksum - remember checksum is NOT in network byte order
    tcp_header = pack('!HHLLBBH' , tcp_source, tcp_dest, tcp_seq, tcp_ack_seq, tcp_offset_res, tcp_flags,  tcp_window) + pack('H' , tcp_check) + pack('!H' , tcp_urg_ptr)
 
    # final full packet - syn packets dont have any data
    packet = ip_header + tcp_header + user_data
 
    #Send the packet finally - the port specified has no effect
    s.sendto(packet, (dest_ip , 0 ))



if __name__ == '__main__':
    
    try:
        f = open(FILE_PATH+FILE_NAME)
        for i in f.readlines():
            m = re.search(r"(\d+\.\d+\.\d+\.\d+\.\d+) \> (\d+\.\d+\.\d+\.\d+\.\d+)",i)
            if m is not None:
                src_addr_tmp    =  m.group(1)
                dst_addr_tmp    =  m.group(2)
             
                src_add = '.'.join(src_addr_tmp.split('.')[0:4])
                dst_add = '.'.join(dst_addr_tmp.split('.')[0:4])
                src_port = int(src_addr_tmp.split('.')[4])
                dst_port = int(dst_addr_tmp.split('.')[4])
                packet_builder(src_add,dst_add,src_port,dst_port)
            else:
                pass
    except IOError:
        sys.stderr.write("Please Check Your Input Data File.It May Displace or Not Located")
        
